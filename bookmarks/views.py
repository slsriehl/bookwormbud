import requests, json, re, time, datetime
from datetime import date
from datetime import datetime as datetime_class
from lxml import html

from django.forms.formsets          import formset_factory
from django.core.urlresolvers       import reverse_lazy
from django.db                      import IntegrityError
from django.http                    import HttpResponse
from django.shortcuts               import redirect, render, render_to_response
from django.contrib                 import messages
from django.shortcuts               import get_object_or_404
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models     import User
from django.template.defaultfilters import slugify
from django.core                    import serializers
from django.views.decorators.csrf   import csrf_exempt

from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.views.generic.list import ListView


from bookmarks.models       import Article, MyTag
from bookmarks.forms        import ArticleForm, TagForm, TagSelectForm, EditArticleForm, AddedDateCalendarForm, LastViewedDateCalendarForm
from bookmarks.functions    import get_tags_for_user, andify_list, replace_html_codes, now, update_tags, ensure_http, get_articles_for_user, string_to_datetime

#----------------------------------------------------------------------------------------------------



@csrf_exempt
def ajax_article_recently_viewed(request):
    article_id = request.POST['articleID']
    try:
        article = Article.objects.get(id=article_id)
        article.last_read = now()
        article.viewed_count += 1
        article.save()
    except:
        print('failed')
    return redirect('bookmarks:silo')


class ArticleListView(ListView):
    model = Article
    template_name = 'bookmarks/silo.html'

    def get_queryset(self):
        return Article.objects.current

    def search_by_tags(self, tag_slugs=None, articles=None):
        current_user = self.request.user
        #if there aren't any articles passed in, they must have all been filtered out by the other search criteria. end now
        if not articles:
            return []
        found = []
        tag_objects_chosen = []
        for tag_slug in tag_slugs:
            tag_objects_chosen.append(MyTag.objects.get(slug=tag_slug, user=current_user))
        for article in articles:
            article_tags = article.tags.all()
            count = 0
            if article_tags:
                for tag in tag_objects_chosen:
                    if tag in article_tags:
                        count += 1
                if count:
                    found.append({'article': article, 'count': count})
        counts = []
        for this_dict in found:
            current_count = this_dict['count']
            if current_count not in counts:
                counts.append(current_count)
        counts.sort()
        counts.reverse()
        final_list = []
        for cnt in counts:
            for this_dict in found:
                if this_dict['count'] == cnt:
                    final_list.append(this_dict['article'])
        return final_list

    def get(self, request):
        articles    = Article.objects.current().filter(user=request.user).extra(\
                                                select={'lower_title':'lower(title)'}).order_by('-last_read', 'lower_title')
        added_date_form         = AddedDateCalendarForm()
        last_viwed_date_form    = LastViewedDateCalendarForm()
        tags        = get_tags_for_user(user=request.user)
        if request.GET:
            pk_to_delete = request.GET.get('delete')
            pk_to_archive = request.GET.get('archive')
            article=None
            if pk_to_delete:
                try:
                    article = Article.objects.get(id=pk_to_delete)
                    article.delete()
                except:
                    pass
            if pk_to_archive:
                try:
                    article = Article.objects.get(id=pk_to_archive)
                    article.archive()
                except:
                    pass
        return render(request, 'bookmarks/silo.html', locals())

    def post(self, request, *args, **kwargs):
        tags                    = get_tags_for_user(user=request.user)
        tags_chosen             = request.POST.getlist('selected_tags')
        added_date_form         = AddedDateCalendarForm()
        last_viwed_date_form    = LastViewedDateCalendarForm()
        articles                = Article.objects.current().filter(user=request.user)
        """if not articles...cut off here"""
        added_start_date = request.POST.getlist('added_start_date')[0]
        if added_start_date:
            added_start_datetime = string_to_datetime(added_start_date)
            if articles:
                articles = articles.filter(created__gte=added_start_datetime)

        added_end_date = request.POST.getlist('added_end_date')[0]
        if added_end_date:
            added_end_datetime = string_to_datetime(added_end_date)
            if articles:
                articles = articles.filter(created__lte=added_end_datetime)

        last_viewed_start = request.POST.getlist('last_viewed_start_date')
        try:
            last_viewed_start_date = last_viewed_start[0]
            if last_viewed_start_date:
                last_viewed_start_datetime = string_to_datetime(last_viewed_start_date)
                if articles:
                    articles = articles.filter(last_read__gte=last_viewed_start_datetime)
        except:
            pass

        last_viewed_end = request.POST.getlist('last_viewed_end_date')
        try:
            last_viewed_end_date = last_viewed_end[0]
            if last_viewed_end_date:
                last_viewed_end_datetime = string_to_datetime(last_viewed_end_date)
                if articles:
                    articles = articles.filter(last_read__lte=last_viewed_end_datetime)
        except:
            pass

        if tags_chosen:
            articles = self.search_by_tags(tag_slugs=tags_chosen, articles=articles)
            if not articles:
                tags_chosen_string  = andify_list(tags_chosen)
                articles            = Article.objects.current().filter(user=request.user)
                messages.warning(request, 'No articles matched {tags_chosen_string}!'.format(tags_chosen_string=tags_chosen_string))
        return render(request, 'bookmarks/silo.html', locals())


class ArticleCreateView(CreateView):
    model = Article
    template_name = 'bookmarks/add_article.html'
    form_class = ArticleForm

    def get_context_data(self, **kwargs):
        context = super(ArticleCreateView, self).get_context_data(**kwargs)
        context['tags'] = get_tags_for_user(user=self.request.user)
        return context

    def form_invalid(self, **kwargs):
        return self.render_to_response(self.get_context_data(**kwargs))

    def post(self, request, *args, **kwargs):
        self.object = None
        user        = request.user
        form_class = self.get_form_class()
        form_name = 'form'
        form = self.get_form(form_class)
        if form.is_valid():
            tags_chosen = request.POST.getlist('existing_tags')
            tags_chosen.extend(request.POST.getlist('new_tags'))
            form_class = self.get_form_class()
            form_name = 'form'
            form = self.get_form(form_class)
            article = form.save(commit=False)
            article.save()
            update_tags(article, tags_chosen, user)
            return self.form_valid(form)
        else:
            return self.form_invalid(**{form_name: form})


class ArticleUpdateView(UpdateView):
    model           = Article
    form_class      = EditArticleForm
    template_name   = 'bookmarks/edit_article.html'

    def get_context_data(self, **kwargs):
        context = super(ArticleUpdateView, self).get_context_data(**kwargs)
        context['tags'] = get_tags_for_user(user=self.request.user)
        context['articletagsdata'] = serializers.serialize('json', self.object.tags.all())
        # x = "['a', 'b']"
        # jsondump = json.dumps(x)
        # context['jsondump'] = jsondump
        return context

    # def add_tags(self, article, tags_to_add):
    #     for tag in tags_to_add:
    #         article.tags.add(tag)

    def form_invalid(self, **kwargs):
        return self.render_to_response(self.get_context_data(**kwargs))

    def post(self, request,  *args, **kwargs):
        self.object = None
        article_in_question = Article.objects.get(pk=self.kwargs['pk'])
        form_class = self.get_form_class()
        form_name = 'form'
        form = self.get_form(form_class)
        form.instance = article_in_question
        if form.is_valid():
            tags_chosen = request.POST.getlist('existing_tags')
            tags_chosen.extend(request.POST.getlist('new_tags'))
            article = form.save(commit=False)
            article.save()
            update_tags(article, tags_chosen, request.user)
            return self.form_valid(form)
        else:
            return self.form_invalid(**{form_name: form})


class ArticleDeleteView(DeleteView):
    model = Article
    success_url = reverse_lazy('bookmarks:silo')


class ArchivesListView(ListView):
    model = Article
    template_name = 'bookmarks/archives_list.html'

    def get(self, request):
        articles = Article.objects.archived().filter(user=request.user)
        if request.GET:
            pk_to_delete = request.GET.get('delete')
            pk_to_unarchive = request.GET.get('unarchive')
            article=None
            if pk_to_delete:
                try:
                    article = Article.objects.get(id=pk_to_delete)
                    article.delete()
                except:
                    pass
            if pk_to_unarchive:
                try:
                    article = Article.objects.get(id=pk_to_unarchive)
                    article.unarchive()
                except:
                    pass
        return render(request, 'bookmarks/archives_list.html', locals())

    def get_queryset(self):
        return Article.objects.archived()

    def get_context_data(self, request, *args, **kwargs):
        context = super(ArchivesListView, self).get_context_data(**kwargs)
        context['articles'] = Article.objects.archived()
        return context


@login_required
def archive_article(request, article_id):
    article = Article.objects.get(id=article_id)
    article.archive()
    return redirect('bookmarks:silo')


@login_required
def unarchive_article(request, article_id):
    article = Article.objects.get(id=article_id)
    article.unarchive()
    return redirect('bookmarks:archives-list')


@login_required
def manage_tags(request, template_name='bookmarks/manage_tags.html'):
    user            =request.user
    existing_tags   = get_tags_for_user(user=user)
    if request.POST:

        tags_to_add = request.POST.getlist('added_tags')
        for tag_slug in tags_to_add:
            try:
                tag = MyTag.objects.get(slug=tag_slug, user=user)
            except:
                tag = MyTag(name=tag_slug, user=user)
                tag.save()

        tags_to_delete  = request.POST.getlist('deleted_tags')
        for tag_slug in tags_to_delete:
            try:
                tag = MyTag.objects.get(slug=tag_slug, user=user)
                tag.delete()
            except:
                pass

    tags = get_tags_for_user(user=user)
    return render(request, template_name, locals())


@login_required
def topic_list(request):
    user = request.user
    return redirect('bookmarks:silo')
