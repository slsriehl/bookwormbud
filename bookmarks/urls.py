from django.conf.urls import url, include
from django.contrib.auth.decorators import login_required

from bookmarks import views
from bookmarks.models import Article

urlpatterns = [

    url(r'^silo/$',			                        login_required(views.ArticleListView.as_view()),        name='silo'),
    url(r'^new-article/$',	                        login_required(views.ArticleCreateView.as_view()),      name='new-article'),
    url(r'^edit-article/(?P<pk>[0-9]+)/$',          login_required(views.ArticleUpdateView.as_view()),      name='edit-article'),
    url(r'^archive-article/(?P<article_id>\d+)/$',  views.archive_article,                                  name='archive-article'),
    url(r'^unarchive-article/(?P<article_id>\d+)/$',views.unarchive_article,                                name='unarchive-article'),
    url(r'^archives/$',	                            login_required(views.ArchivesListView.as_view()),       name='archives-list'),
    url(r'^manage-tags/$',                          views.manage_tags,                                      name='manage-tags'),
    url(r'^article_viewed/$',                       views.ajax_article_recently_viewed,                     name='recently-viewed-update'),
    url(r'^topics/$',                               views.topic_list,                                       name='topic-list'),

]
