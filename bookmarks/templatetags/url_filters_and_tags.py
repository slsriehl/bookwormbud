from django import template

register = template.Library()


@register.filter
def ensure_http(url):
    if url.startswith('http://'):
        return url
    elif url.startswith('https://'):
        return url
    else:
        return ('http://' + url)