from datetime import timedelta, datetime as datetime_class

from django.db                      import IntegrityError
from django.template.defaultfilters import slugify


from bookmarks.models   import Article, MyTag


# http://stackoverflow.com/questions/1937622/convert-date-to-datetime-in-python
def string_to_datetime(date_string):
    return datetime_class.strptime(date_string, '%m/%d/%Y')

def date_N_days_ago(n):
    return datetime_class.now() - timedelta(days=n)

def myall(model):
    temp = None
    exec("temp = {}.objects.all()".format(model))
    return temp

def andify_list(the_list):
    def _string_clean(l):
        clean_list = []
        for item in l:
            clean_list.append(str(item))
        return clean_list

    the_list = _string_clean(the_list)
    if len(the_list) == 0:
        return ""
    if len(the_list) == 1:
        return the_list[0]
    if len(the_list) == 2:
        return the_list[0] + ' and ' + the_list[1]
    head = the_list[:-1]
    head_string = ", ".join(head)
    return head_string + " and " + the_list[-1]



def replace_html_codes(title):
    pass


def ensure_http(url):
    if url.startswith('http://'):
        return url
    elif url.startswith('https://'):
        return url
    else:
        return ('http://' + url)


def now():
    return datetime.datetime.now()


def update_tags(article, unknown_tags, user):
    updated_tags = []
    for thing in unknown_tags:
        try:
            slug = slugify(thing)
            existing_tag = MyTag.objects.get(slug=slug, user=user)
            updated_tags.append(existing_tag)
        except:
            try:
                new_tag = MyTag(name=thing, user=user)
                new_tag.save()
                updated_tags.append(new_tag)
            except IntegrityError:
                pass
    current_tags = article.tags.all()
    for t in current_tags:
        if t not in updated_tags:
            article.tags.remove(t)
    for t in updated_tags:
        if t not in current_tags:
            article.tags.add(t)


def get_tags_for_user(user=None, username=None):
    try:
        return  MyTag.objects.all().order_by('name').filter(user=user)
    except ValueError:
        user = User.objects.get(username=username)
        return  MyTag.objects.all().order_by('name').filter(user=user)


def get_articles_for_user(user=None, username=None):
    try:
        return  Article.objects.filter(user=user).order_by('-last_read')
    except ValueError:
        user = User.objects.get(username=username)
        return  Article.objects.filter(user=user).order_by('-last_read')
