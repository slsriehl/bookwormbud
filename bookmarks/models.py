

from django.db import models
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse

from taggit.managers import TaggableManager
from taggit.models import TagBase, GenericTaggedItemBase
from taggit.utils import require_instance_manager

#----------------------------------------------------------------------------------------------------

class ArticleQueryset(models.query.QuerySet):
    def archived(self):
        return self.filter(archived=True)
    def current(self):
        return self.filter(archived=False)
    def category(self, this_category):
        return self.filter(category=this_category)
    def all_categories(self):
        categories = self.values('category').distinct()
        filtered_categories = [this_dict['category'] for this_dict in categories if this_dict['category']]
        return filtered_categories

#----------------------------------------------------------------------------------------------------

class ArticleManager(models.Manager):
    def get_queryset(self):
        return ArticleQueryset(self.model, using=self._db)
    def archived(self):
        return self.get_queryset().archived()
    def current(self):
        return self.get_queryset().current()
    def category(self):
        return self.get_queryset().category()
    def all_categories(self):
        return self.get_queryset().all_categories()

#----------------------------------------------------------------------------------------------------


class MyTag(TagBase):
    user = models.ForeignKey(User)

    def __unicode__(self):
        return self.slug

    def save(self, *args, **kwargs):
        slugged_name    = self.slugify(self.name)
        self.name       = slugged_name
        super(MyTag, self).save( *args, **kwargs)

#----------------------------------------------------------------------------------------------------

class MyTaggedManager(GenericTaggedItemBase):
    tag = models.ForeignKey(MyTag, related_name="%(app_label)s_%(class)s_items")

#----------------------------------------------------------------------------------------------------
#template for the table
class Article(models.Model):
    url             = models.CharField(max_length=256, unique=True)
    title           = models.CharField(max_length=256, default="None", blank=True)
    created         = models.DateTimeField(auto_now_add=True)
    last_read       = models.DateTimeField(null=True, blank=True)
    viewed_count    = models.IntegerField(default=0)
    archived        = models.BooleanField(default=False, blank=True)
    user            = models.ForeignKey(User, null=True, blank=True)

    #rules that govern working of table
    def __unicode__(self):
        return self.title

    def archive(self):
        self.archived = True
        self.save()

    def unarchive(self):
        self.archived = False
        self.save()

    objects = ArticleManager()

    tags = TaggableManager(through=MyTaggedManager)

    def get_absolute_url(self):
        return reverse('bookmarks:silo')

#----------------------------------------------------------------------------------------------------

class Topic(models.Model):
    title       = models.CharField(max_length=256)
    description = models.TextField(null=True, blank=True)

#----------------------------------------------------------------------------------------------------

class ArticleTopic(models.Model):
    class Meta:
        unique_together = (('topic', 'article',))

    topic   = models.ForeignKey('bookmarks.Topic')
    article = models.ForeignKey('bookmarks.Article')
