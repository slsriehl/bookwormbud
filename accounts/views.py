from django.shortcuts   import render
from django.http        import HttpResponse, HttpResponseRedirect
from django.template    import RequestContext
from django.shortcuts   import render, redirect
from django.contrib     import messages
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from django.template.response import TemplateResponse
from django.core import serializers
from django.core.urlresolvers import reverse

from accounts.forms import UserForm, LoginForm

#------------------------------------------------------------------------------------------------------------------------------------------------------

def register(request):
    context = RequestContext(request)
    registered = False
    if request.method == 'POST':
        user_form = UserForm(data=request.POST)
        username = request.POST['username']
        password = request.POST['password']
        if user_form.is_valid():
            user = user_form.save()
            user.set_password(user.password)
            user.save()
            registered = True
            user = authenticate(username=username, password=password)
            login(request, user)
            messages.success(request, 'You have been registered {}!'.format(user.username))
            return redirect('bookmarks:silo')

        # if user is already registered but forgot, log them in silently
        user = authenticate(username=username, password=password)
        if user:
            if user.is_active:
                login(request, user)
                return redirect('bookmarks:silo')

        return render(request, 'accounts/register.html', locals())

    user_form = UserForm()
    template_name = 'accounts/register.html'
    response = TemplateResponse(request, 'accounts/register.html', locals())
    return response

#------------------------------------------------------------------------------------------------------------------------------------------------------

def user_logout(request):
    logout(request)
    return HttpResponseRedirect('/accounts/login')

#------------------------------------------------------------------------------------------------------------------------------------------------------

def user_login(request):
    context = RequestContext(request)
    if request.method == 'POST':
        form = LoginForm(request.POST)
        username = request.POST['username']
        password = request.POST['password']

        user = authenticate(username=username, password=password)

        if user:
            if user.is_active:
                login(request, user)
                return redirect('bookmarks:silo')
            else:
                return HttpResponse("Your Sitename account is disabled.")

        else:
            return render(request, 'accounts/login.html', locals())
    else:

        template_name = 'accounts/login.html'
        return render(request, 'accounts/login.html', locals())

#------------------------------------------------------------------------------------------------------------------------------------------------------
